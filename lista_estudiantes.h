/**********************************************************************
    Instituto Tecnológico de Costa Rica
    Estructuras de Datos IC-2001
    II Semestre 2019
    Profesora: Samanta Ramijan Carmiol
    Ejemplos Prácticos: Lista de Estudiantes
    Estudiante: Andres Lopez Sanchez
**********************************************************************/
//Definición de Macros
#define LONGITUD_MAXIMA_NOMBRE 50 
#define LONGITUD_MAXIMA_CARNET 12

//Definición de Estructuras
typedef struct nodo_estudiante
{
	int carnet;
	char *nombre;

	struct nodo_estudiante *ref_siguiente;
}nodo_estudiante;

typedef struct lista_estudiantes
{
	nodo_estudiante *ref_inicio;
	int cantidad;
}lista_estudiantes;

//Definición de Funciones
/*-----------------------------------------------------------------------
	crear_nodo
	Entradas: No recibe parámetros
	Salidas: Retorna un puntero de tipo nodo_estudiante al nodo creado
	Funcionamiento: 
		- Solicita al usuario ingresar Nombre y Carnet.
		- Crea un puntero de tipo nodo_estudiante
		- Le asigna al nodo el nombre y carnet ingresados.
		- El nodo apunta a NULL.
		- retorna el puntero al nuevo nodo.
-----------------------------------------------------------------------*/
nodo_estudiante* crear_nodo();
/*-----------------------------------------------------------------------
	inicializar_lista
	Entradas: No recibe parametros.
	Salidas: No retorna nada. 
	Funcionamiento: 
	    - Se guarda un espacion memoria para ref_lista.*
	    - Se le asigna una cantidad de 0.
	    - El nodo (ref_lista) apunta a ref_inicio que es igualado a NULL.
-----------------------------------------------------------------------*/
void inicializar_lista();
/*-----------------------------------------------------------------------
	insertar_inicio
	Entradas: La funcion nodo_estudiante* nuevo.
	Salidas: No retorna nada.
	Funcionamiento: 
	    - Si ref_lista es igual a NULL llama a la funcion inicializar_lista.
	    - El nuevo nodo apunta ref_siguiente y reciben a ref_lista que apunta a ref_inicio.
	    - ref_lista que apunta a ref_inicio reciben el valor del nodo nuevo.
	    - Se le suma 1 a la cantidad e ref_lista.
-----------------------------------------------------------------------*/
void insertar_inicio(nodo_estudiante* nuevo);
/*-----------------------------------------------------------------------
	insertar_final
	Entradas:  La funcion nodo_estudiante* nuevo.
	Salidas:  No retorna nada.
	Funcionamiento:
	   - Si ref_lista es igual a NULL llama a la funcion inicializar_lista.
	   - Si la cantidad de ref_lista es igual a cero ,se llama a insertar_inicio(nuevo).
	   - Se crea un nodo llamado temporal.
	   - temporal recibe el valor de ref_lista que apunta a ref_inicio.
	   - Mienras que ref_siguiente de temporal sea differente a Null , temporal va a ser igual al ref_siguente de temporal.
	   - Se le asigna nuevo al ref_siguente de temporal.
	   - Se le suma 1 a la cantidad de ref_lista.
-----------------------------------------------------------------------*/
void insertar_final(nodo_estudiante* nuevo);
/*-----------------------------------------------------------------------
	borrar_por_indice
	Entradas: La variable indice.
	Salidas: No retorna nada.
	Funcionamiento: 
	    - Se crea un nodo llamado temporal.
	    - Se le asigna el valor NUll.
	    - Se declara la variable cont y se le asigna 0.
	    - Si ref_inicio de ref_lista es igual a null imprime "la lista esta vacia".
	        - Sino , si indice es igual o mayor a la cantidad de ref_lista se imprime "El indice ingresado no es valido".
	                - Sino, se crea un nodo temporal , se le gurada memoria a este nodo , temporal recibe el ref_inicio de ref_lista.
	                    - si indice es igual a 0 , ref_inicio de ref_lista recibe el valor de ref_siguente de temporal, se le resta 1 a la cantidad de ref_lista y se libera el espacio de temporal.
	                    - Sino, mientras ref_siguente de temporal sea differente a null se le suma 1 a cont.
	                        - si, cont es igual a indice a ref_siguente de temporal se le asigna el ref_siguente del ref_siguente de temporal y se le resta 1 a cantidad de ref_lista.
	                        - A temporal se le asigna el ref_siguente de temporal.
	                        
-----------------------------------------------------------------------*/
void borrar_por_indice(int indice);
 /*-----------------------------------------------------------------------
	buscar_por_indice
	Entradas: ?
	Salidas: ?
	Funcionamiento: ?
-----------------------------------------------------------------------*/
nodo_estudiante* buscar_por_indice(int indice);

 /*-----------------------------------------------------------------------
	validar_carnets
	Entradas: carnet_almacenado y carnet_ingresado.
	Salidas: No retorna nada.
	Funcionamiento: 
	    - si carnet_almacenado es igual a carnet_ingresado imprime " el carnet ingresado es el correcto".
	    - sino, imprime "el carnet ingresado es incorrecto".
-----------------------------------------------------------------------*/
void validar_carnets(int carnet_almacenado, int carnet_ingresado);
 /*-----------------------------------------------------------------------
	menu
	Entradas: No tiente Entradas.
	Salidas: retorna impresiones.
	Funcionamiento: 
	    - se abre un menu para que el usuario elija que opcion desea realizar.
-----------------------------------------------------------------------*/
void menu();
 /*-----------------------------------------------------------------------
	main
	Entradas: No tiene entradas.
	Salidas: retorna un 0.
	Funcionamiento: 
	    - llama a la funcion menu().
-----------------------------------------------------------------------*/
int main();
 /*-----------------------------------------------------------------------
	get_user_input
	Entradas: entra size_t y max_size.
	Salidas: retorna buffer.
	Funcionamiento: 
	    - Reserva memoria para buffer y sino puede le da el error.
	    - 
-----------------------------------------------------------------------*/
char* get_user_input(size_t max_size);
 /*-----------------------------------------------------------------------
	get_user_numerical_input
	Entradas: Entra size_t y max_size
	Salidas: retorna numerical imput.
	Funcionamiento: 
	    - converts get_user_input(max_size) into a int.
-----------------------------------------------------------------------*/
int get_user_numerical_input(size_t max_size);